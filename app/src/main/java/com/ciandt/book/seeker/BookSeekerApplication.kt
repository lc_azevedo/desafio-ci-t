package com.ciandt.book.seeker

import android.app.Application
import com.ciandt.book.seeker.data.api.ItunesService
import com.ciandt.book.seeker.di.listingModule
import com.orhanobut.hawk.Hawk
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class BookSeekerApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@BookSeekerApplication)
            modules(listOf(listingModule))
        }

        Hawk.init(this).build()
    }
}