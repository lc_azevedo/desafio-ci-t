package com.ciandt.book.seeker.ui.common

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.ciandt.book.seeker.R
import com.ciandt.book.seeker.data.model.Book
import com.ciandt.book.seeker.ui.listing.ListingFragmentDirections
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_book_listing.view.*

class BookListingAdapter(val books: MutableList<Book>) :
    RecyclerView.Adapter<BookListingViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookListingViewHolder {
        val layout =
            LayoutInflater.from(parent.context).inflate(R.layout.item_book_listing, parent, false)
        return BookListingViewHolder(layout)
    }

    override fun getItemCount() = books.size

    override fun onBindViewHolder(holder: BookListingViewHolder, position: Int) {
        holder.bind(books[position])
    }

    fun setItems(newBooks: List<Book>) {
        books.apply {
            clear()
            addAll(newBooks)
        }
        notifyDataSetChanged()
    }
}

class BookListingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(book: Book) {
        with(itemView) {

            Picasso.get()
                .load(book.artworkUrl100)
                .placeholder(R.drawable.ic_book)
                .error(R.drawable.ic_book)
                .into(bookListingImageCover)
            bookListingTextTitle.text = book.trackName
            bookListingTextAuthor.text = book.artistName

            setOnClickListener {
                val action =
                    ListingFragmentDirections.bookListItemToDetailsFragment(book)
                Navigation.findNavController(itemView).navigate(action)
            }
        }
    }
}