package com.ciandt.book.seeker.ui

import android.os.Bundle
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.ciandt.book.seeker.R
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme)
        setContentView(R.layout.activity_home)

        setupViews()
    }

    override fun setupViews() {
        bottomNavigationHome.setupWithNavController(
            Navigation.findNavController(
                this,
                R.id.navigationHostHome
            )
        )
    }

    override fun onSupportNavigateUp() = findNavController(R.id.navigationHostHome).navigateUp()
}
