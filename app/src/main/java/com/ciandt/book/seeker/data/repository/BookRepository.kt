package com.ciandt.book.seeker.data.repository

import com.ciandt.book.seeker.data.source.BooksDataSource

open class BookRepository(val booksDataSource: BooksDataSource) {
    suspend fun getBooks(term: String, entity: String) = booksDataSource.getBooks(term, entity)
}