package com.ciandt.book.seeker.data.api

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ItunesService {

    lateinit var api: ItunesApi

    fun createApi(baseUrl: String = "https://itunes.apple.com/") {
        val client = OkHttpClient.Builder().build()
        val gson = GsonBuilder().setLenient().create()
        val retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        api = retrofit.create(ItunesApi::class.java)

    }

}