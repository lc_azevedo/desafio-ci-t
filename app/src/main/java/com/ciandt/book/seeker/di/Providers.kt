package com.ciandt.book.seeker.di

import android.content.Context
import com.ciandt.book.seeker.BuildConfig
import com.ciandt.book.seeker.data.api.ItunesApi
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

fun provideApi(context: Context): ItunesApi {
    val client = OkHttpClient.Builder().build()
    val gson = GsonBuilder().setLenient().create()
    val retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.baseUrl)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    return retrofit.create(ItunesApi::class.java)

}

fun provideMockApi(context: Context): ItunesApi {
    val client = OkHttpClient.Builder().build()
    val gson = GsonBuilder().setLenient().create()
    val retrofit = Retrofit.Builder()
        .baseUrl("http://localhost:8080/")
        .client(client)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    return retrofit.create(ItunesApi::class.java)
}