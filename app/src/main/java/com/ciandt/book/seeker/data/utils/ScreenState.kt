package com.ciandt.book.seeker.data.utils

sealed class ScreenState

class SuccessState(val data: Any?) : ScreenState()
class ErrorState(val error: String) : ScreenState()
class LoadingState() : ScreenState()