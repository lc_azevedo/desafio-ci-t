package com.ciandt.book.seeker.ui.listing


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ciandt.book.seeker.R
import com.ciandt.book.seeker.data.model.Book
import com.ciandt.book.seeker.data.model.BookSearchResult
import com.ciandt.book.seeker.data.utils.ErrorState
import com.ciandt.book.seeker.data.utils.LoadingState
import com.ciandt.book.seeker.data.utils.SuccessState
import com.ciandt.book.seeker.ui.BaseFragment
import com.ciandt.book.seeker.ui.common.BookListingAdapter
import com.ciandt.book.seeker.ui.common.ListingSearchViewModel
import kotlinx.android.synthetic.main.fragment_listing.*
import kotlinx.android.synthetic.main.fragment_search.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ListingFragment : BaseFragment() {

    val viewModel: ListingSearchViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_listing, container, false)
    }

    override fun setUpViews() {
        super.setUpViews()
        (activity as AppCompatActivity).setSupportActionBar(bookSearchToolbar)
        bookListingButtonTryAgain.setOnClickListener { setUpViewModels() }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setUpViews()
        setUpViewModels()
        setUpObservables()
    }

    override fun setUpViewModels() {
        viewModel.getBooks()
    }

    override fun setUpObservables() {
        viewModel.screenState.observe(this, Observer {
            when (it) {
                is LoadingState -> {
                    bookListingFlipperState.displayedChild = LOADING_CHILD
                }
                is SuccessState -> {
                    setUpBookList((it.data as BookSearchResult).results)
                    bookListingFlipperState.displayedChild = RESULT_CHILD
                }
                is ErrorState -> {
                    bookListingFlipperState.displayedChild = ERROR_CHILD
                }
            }
        })
    }

    private fun setUpBookList(books: List<Book>) {

        val linearLayoutManager = LinearLayoutManager(requireContext())

        with(bookListingList) {
            adapter = BookListingAdapter(books.toMutableList())
            layoutManager = linearLayoutManager

            val dividerItemDecoration = DividerItemDecoration(
                bookListingList.context,
                linearLayoutManager.getOrientation()
            )
            addItemDecoration(dividerItemDecoration)
        }
    }
}