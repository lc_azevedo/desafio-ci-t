package com.ciandt.book.seeker.data.api

import com.ciandt.book.seeker.data.model.BookSearchResult
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ItunesApi {
    @GET("/search")
    suspend fun getBooks(
        @Query("term") term: String,
        @Query("entity") entity: String
    ): Response<BookSearchResult>
}