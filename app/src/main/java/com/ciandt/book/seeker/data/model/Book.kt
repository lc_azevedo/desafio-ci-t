package com.ciandt.book.seeker.data.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.lang.reflect.InvocationTargetException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
data class Book(
    @SerializedName("artworkUrl60")
    @Expose
    var artworkUrl60: String? = null,
    @SerializedName("artworkUrl100")
    @Expose
    var artworkUrl100: String? = null,
    @SerializedName("artistViewUrl")
    @Expose
    var artistViewUrl: String? = null,
    @SerializedName("trackCensoredName")
    @Expose
    var trackCensoredName: String? = null,
    @SerializedName("fileSizeBytes")
    @Expose
    var fileSizeBytes: Int? = null,
    @SerializedName("trackViewUrl")
    @Expose
    var trackViewUrl: String? = null,
    @SerializedName("releaseDate")
    @Expose
    var releaseDate: String? = null,
    @SerializedName("genreIds")
    @Expose
    var genreIds: List<String>? = null,
    @SerializedName("currency")
    @Expose
    var currency: String? = null,
    @SerializedName("formattedPrice")
    @Expose
    var formattedPrice: String? = null,
    @SerializedName("artistIds")
    @Expose
    var artistIds: List<Int>? = null,
    @SerializedName("description")
    @Expose
    var description: String? = null,
    @SerializedName("artistId")
    @Expose
    var artistId: Int? = null,
    @SerializedName("artistName")
    @Expose
    var artistName: String? = null,
    @SerializedName("genres")
    @Expose
    var genres: List<String>? = null,
    @SerializedName("kind")
    @Expose
    var kind: String? = null,
    @SerializedName("price")
    @Expose
    var price: Double? = null,
    @SerializedName("trackId")
    @Expose
    var trackId: Int? = null,
    @SerializedName("trackName")
    @Expose
    var trackName: String? = null
) : Parcelable {
    // Fixes the new line character
    fun getDescriptionFormatted() = description
        ?.replace("<br />", System.lineSeparator())
        ?.replace("<br/>", System.lineSeparator())
        ?: ""

    fun getDateFormatted(): String {
        val originalDate = releaseDate ?: ""
        try {
            val originalFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val date = originalFormat.parse(originalDate)
            val newFormat = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
            date?.let {
                return newFormat.format(date)
            }
            return originalDate
        } catch (exception: ParseException) {
            return originalDate
        }
    }
}