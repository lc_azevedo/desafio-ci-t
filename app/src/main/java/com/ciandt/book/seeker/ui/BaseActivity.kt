package com.ciandt.book.seeker.ui

import androidx.appcompat.app.AppCompatActivity

open class BaseActivity : AppCompatActivity() {
    open fun setupViews() {}
}