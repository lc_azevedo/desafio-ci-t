package com.ciandt.book.seeker.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ciandt.book.seeker.R
import com.ciandt.book.seeker.ui.BaseFragment
import com.google.android.material.chip.Chip
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_details.*

class DetailsFragment : BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()
    }

    override fun setUpViews() {
        super.setUpViews()
        arguments?.let {
            val args = DetailsFragmentArgs.fromBundle(it)
            with(args.book) {
                Picasso.get()
                    .load(artworkUrl100)
                    .placeholder(R.drawable.ic_book)
                    .error(R.drawable.ic_book)
                    .into(bookDetailsImageCover)
                bookDetailsTextTitle.text = trackName
                bookDetailsTextAuthor.text = artistName
                bookDetailsDescriptionTextValue.text = getDescriptionFormatted()
                bookDetailsReleaseTextValue.text = getDateFormatted()
                bookDetailsPriceTextValue.text = formattedPrice
                genres?.let {
                    setGenres(it)
                }
            }
        }
    }


    private fun setGenres(genres: List<String>) {
        genres.forEach {
            bookDetailsCategoryChipGroup.addView(
                Chip(bookDetailsCategoryChipGroup.context).apply {
                    text = it
                    isClickable = false
                    isCheckable = false
                })
        }
    }
}