package com.ciandt.book.seeker.di

import com.ciandt.book.seeker.data.repository.BookRepository
import com.ciandt.book.seeker.data.source.BooksDataSource
import com.ciandt.book.seeker.ui.common.ListingSearchViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val listingModule = module {
    single { provideApi(androidApplication()) }
    single { BooksDataSource(get()) }
    single { BookRepository(get()) }
    viewModel { ListingSearchViewModel(get()) }
}

val mockListingModule = module {
    single { provideMockApi(androidApplication()) }
    single { BooksDataSource(get()) }
    single { BookRepository(get()) }
    viewModel { ListingSearchViewModel(get()) }
}