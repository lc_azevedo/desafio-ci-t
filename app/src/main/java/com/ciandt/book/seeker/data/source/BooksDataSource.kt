package com.ciandt.book.seeker.data.source

import com.ciandt.book.seeker.data.api.ItunesApi

open class BooksDataSource(val api: ItunesApi) {
    suspend fun getBooks(term: String, entity: String) = api.getBooks(term, entity)
}