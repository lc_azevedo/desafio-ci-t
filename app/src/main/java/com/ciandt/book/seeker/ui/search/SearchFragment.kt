package com.ciandt.book.seeker.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ciandt.book.seeker.R
import com.ciandt.book.seeker.data.model.BookSearchResult
import com.ciandt.book.seeker.data.utils.ErrorState
import com.ciandt.book.seeker.data.utils.LoadingState
import com.ciandt.book.seeker.data.utils.SuccessState
import com.ciandt.book.seeker.ui.BaseFragment
import com.ciandt.book.seeker.ui.common.BookListingAdapter
import com.ciandt.book.seeker.ui.common.ListingSearchViewModel
import kotlinx.android.synthetic.main.fragment_listing.*
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.coroutines.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class SearchFragment : BaseFragment() {

    private val viewModel: ListingSearchViewModel by viewModel()
    private val searchListAdapter = BookListingAdapter(mutableListOf())
    lateinit var searchHistoryArrayAdapter: ArrayAdapter<String>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()
        setUpObservables()
    }

    override fun setUpViews() {
        super.setUpViews()
        (activity as AppCompatActivity).setSupportActionBar(bookSearchToolbar)
        bookSearchButtonTryAgain.setOnClickListener {
            bookSearchView.setQuery(
                bookSearchView.query.toString(),
                true
            )
        }

        bookSearchStateFlipper.displayedChild = EMPTY_CHILD

        val linearLayoutManager = LinearLayoutManager(requireContext())

        with(bookSearchList) {
            adapter = searchListAdapter
            layoutManager = linearLayoutManager

            val dividerItemDecoration = DividerItemDecoration(
                bookSearchList.context,
                linearLayoutManager.getOrientation()
            )
            addItemDecoration(dividerItemDecoration)
        }

        setUpSearchView()
        setUpSearchAutoComplete()
    }

    override fun setUpObservables() {
        super.setUpObservables()
        viewModel.screenState.observe(this, Observer {
            when (it) {
                is LoadingState -> {
                    bookSearchStateFlipper.displayedChild = LOADING_CHILD
                }
                is SuccessState -> {
                    val result = (it.data as BookSearchResult).results
                    searchListAdapter.setItems(result)
                    if (result.isNotEmpty()) {
                        bookSearchStateFlipper.displayedChild = RESULT_CHILD
                    } else {
                        bookSearchStateFlipper.displayedChild = EMPTY_CHILD
                    }
                }
                is ErrorState -> {
                    bookSearchStateFlipper.displayedChild = ERROR_CHILD
                }
            }
        })
    }

    private fun setUpSearchAutoComplete() {
        context?.let {
            val searchAutoComplete =
                bookSearchView.findViewById<SearchView.SearchAutoComplete>(androidx.appcompat.R.id.search_src_text)
            val data = viewModel.getRecentSearch()
            searchHistoryArrayAdapter =
                ArrayAdapter(it, android.R.layout.simple_dropdown_item_1line, data)
            searchAutoComplete.apply {
                setAdapter(searchHistoryArrayAdapter)
                threshold = 0

                setOnItemClickListener { parent, view, position, id ->
                    bookSearchView.setQuery(searchHistoryArrayAdapter.getItem(position), true)
                }

            }
        }
    }

    private fun setUpSearchView() {
        bookSearchView.apply {

            setOnQueryTextListener(object : SearchView.OnQueryTextListener {

                var searchQuery = ""

                override fun onQueryTextSubmit(query: String?): Boolean {
                    onQueryTextChange(query)
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    searchQuery = newText ?: ""

                    GlobalScope.launch(Dispatchers.IO) {
                        delay(DEBOUNCE_DELAY)
                        if (searchQuery.isEmpty()) {
                            withContext(Dispatchers.Main) {
                                searchListAdapter.setItems(emptyList())
                                searchHistoryArrayAdapter.apply {
                                    clear()
                                    addAll(viewModel.getRecentSearch())
                                    notifyDataSetChanged()
                                }
                            }
                        }
                        if (searchQuery == newText) {
                            viewModel.getBooks(newText)
                            viewModel.saveRecentSearch(newText)
                            return@launch
                        }
                    }

                    return true
                }

            })

            setOnCloseListener {
                searchListAdapter.setItems(emptyList())
                true
            }
        }
    }

    companion object {
        const val DEBOUNCE_DELAY = 500L
    }
}