package com.ciandt.book.seeker.ui

import androidx.fragment.app.Fragment

open class BaseFragment : Fragment() {
    open fun setUpViews() {}

    open fun setUpViewModels() {}

    open fun setUpObservables() {}

    companion object {
        const val LOADING_CHILD = 0
        const val RESULT_CHILD = 1
        const val ERROR_CHILD = 2
        const val EMPTY_CHILD = 3
    }
}