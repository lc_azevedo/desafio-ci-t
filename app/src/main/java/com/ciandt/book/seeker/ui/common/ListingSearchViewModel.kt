package com.ciandt.book.seeker.ui.common

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ciandt.book.seeker.data.repository.BookRepository
import com.ciandt.book.seeker.data.utils.ErrorState
import com.ciandt.book.seeker.data.utils.LoadingState
import com.ciandt.book.seeker.data.utils.ScreenState
import com.ciandt.book.seeker.data.utils.SuccessState
import com.orhanobut.hawk.Hawk
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ListingSearchViewModel(val repository: BookRepository) : ViewModel() {
    val screenState = MutableLiveData<ScreenState>()

    fun getBooks(term: String = "kotlin", entity: String = "ibook") {

        screenState.postValue(LoadingState())

        CoroutineScope(Dispatchers.IO).launch {
            try {
                val result = repository.getBooks(term, entity)

                if (result.isSuccessful) {
                    screenState.postValue(SuccessState(result.body()))
                } else {
                    screenState.postValue(ErrorState(result.message()))
                }
            } catch (exception: Exception) {
                screenState.postValue(ErrorState(exception.toString()))
            }
        }
    }

    fun getRecentSearch(): List<String> {
        return if (Hawk.contains(SEARCH_QUERY_KEY)) {
            Hawk.get<MutableList<String>>(SEARCH_QUERY_KEY, mutableListOf()).asReversed()
        } else {
            emptyList()
        }
    }

    fun saveRecentSearch(query: String) {
        if (query.isEmpty()) {
            return
        }
        var recentSearchs = mutableListOf<String>()
        if (Hawk.contains(SEARCH_QUERY_KEY)) {
            recentSearchs = Hawk.get(SEARCH_QUERY_KEY, mutableListOf())

            if (recentSearchs.contains(query)) {
                recentSearchs.remove(query)
            }
        }

        recentSearchs.add(query)
        Hawk.put(SEARCH_QUERY_KEY, recentSearchs.takeLast(SEARCH_HISTORY_SIZE))
    }

    companion object {
        const val SEARCH_QUERY_KEY = "SEARCH_QUERY"
        const val SEARCH_HISTORY_SIZE = 5
    }
}