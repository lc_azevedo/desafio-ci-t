package com.ciandt.book.seeker.home.search

import androidx.test.rule.ActivityTestRule
import com.ciandt.book.seeker.BookSeekerTestApplication
import com.ciandt.book.seeker.home.common.BaseRobot
import com.ciandt.book.seeker.ui.HomeActivity
import org.junit.Rule
import org.junit.Test

class BookSearchTest {

    @Rule
    @JvmField
    val rule = ActivityTestRule(HomeActivity::class.java, true, true)

    @Test
    fun when_startSearch_should_displayEmptyState() {
        BaseRobot {
            clickSearchBottomBar()
        }

        BookSearchRobot {
            isEmptyStateDisplayed()
        }
    }

    @Test
    fun when_startSearch_should_displayResultState() {
        BaseRobot {
            clickSearchBottomBar()
            enqueueSuccessResponse((rule.activity.application as BookSeekerTestApplication).getServer())
            enqueueSuccessResponse((rule.activity.application as BookSeekerTestApplication).getServer())
        }

        BookSearchRobot {
            typeSearchQuery()
            waitDebounce()
            clickListItem()
            isBookDetailsDisplayed()
        }
    }

}