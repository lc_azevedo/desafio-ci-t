package com.ciandt.book.seeker.home.listing

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.ciandt.book.seeker.R
import com.ciandt.book.seeker.home.common.TestData
import com.ciandt.book.seeker.ui.common.BookListingViewHolder

class BookListingRobot(block: BookListingRobot.() -> Unit) {

    init {
        block.invoke(this)
    }

    fun clickListItem() = onView(withId(R.id.bookListingList)).perform(
        RecyclerViewActions.actionOnItemAtPosition<BookListingViewHolder>(
            0,
            ViewActions.click()
        )
    )

    fun clickRetryButton() = onView(withId(R.id.bookListingButtonTryAgain)).perform(click())

    fun isErrorStateDisplayed() = onView(withId(R.id.bookListingStateError)).check(
        matches(
            ViewMatchers.isDisplayed()
        )
    )

    fun isBookListDisplayed() = onView(ViewMatchers.withText(TestData.VALID_BOOK_TITLE)).check(
        matches(
            ViewMatchers.isDisplayed()
        )
    )

    fun isLoadingStateDisplayed() =
        onView(withId(R.id.bookListingStateProgress)).check(
            matches(ViewMatchers.isDisplayed())
        )

    fun isBookDetailsDisplayed() =
        onView(ViewMatchers.withId(R.id.detailsRootLayout)).check(
            matches(
                ViewMatchers.isDisplayed()
            )
        )
}