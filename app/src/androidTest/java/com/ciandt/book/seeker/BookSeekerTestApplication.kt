package com.ciandt.book.seeker

import android.app.Application
import com.ciandt.book.seeker.di.mockListingModule
import com.orhanobut.hawk.Hawk
import okhttp3.mockwebserver.MockWebServer
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

lateinit var mockWebServer: MockWebServer

class BookSeekerTestApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        mockWebServer = MockWebServer()
        mockWebServer.start(8080)

        startKoin {
            androidContext(this@BookSeekerTestApplication)
            modules(listOf(mockListingModule))
        }

        Hawk.init(this).build()

    }

    fun getServer() = mockWebServer
}