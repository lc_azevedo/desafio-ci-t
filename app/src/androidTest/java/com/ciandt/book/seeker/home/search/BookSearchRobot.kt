package com.ciandt.book.seeker.home.search

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.ciandt.book.seeker.R
import com.ciandt.book.seeker.home.common.TestData
import com.ciandt.book.seeker.ui.common.BookListingViewHolder

class BookSearchRobot(block: BookSearchRobot.() -> Unit) {

    init {
        block.invoke(this)
    }

    fun typeSearchQuery() {
        onView(withId(androidx.appcompat.R.id.search_src_text)).perform(replaceText(TestData.BOOK_SEARCH));
    }

    fun clickListItem() = onView(withId(R.id.bookSearchList)).perform(
        RecyclerViewActions.actionOnItemAtPosition<BookListingViewHolder>(
            0,
            ViewActions.click()
        )
    )

    fun isEmptyStateDisplayed() = onView(withId(R.id.bookSearchStateEmpty)).check(
        ViewAssertions.matches(isDisplayed())
    )

    fun isLoadingStateDisplayed() = onView(withId(R.id.bookSearchStateLoading)).check(
        ViewAssertions.matches(isDisplayed())
    )

    fun isBookDetailsDisplayed() =
        onView(ViewMatchers.withId(R.id.detailsRootLayout)).check(
            ViewAssertions.matches(
                isDisplayed()
            )
        )

    fun waitDebounce() = Thread.sleep(500)
}