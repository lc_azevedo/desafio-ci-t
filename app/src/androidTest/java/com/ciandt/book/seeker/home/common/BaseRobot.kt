package com.ciandt.book.seeker.home.common

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.ciandt.book.seeker.R
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import java.util.concurrent.TimeUnit

class BaseRobot(block: BaseRobot.() -> Unit) {
    init {
        block.invoke(this)
    }

    fun enqueueSuccessResponse(server: MockWebServer) {
        server.enqueue(MockResponse().setResponseCode(200).setBody(TestData.LISTING_RESPONSE))
    }

    fun enqueueSuccessResponseWithThrottle(server: MockWebServer) {
        server.enqueue(
            MockResponse().setResponseCode(200).setBody(TestData.LISTING_RESPONSE).throttleBody(
                100,
                10,
                TimeUnit.SECONDS
            )
        )
    }

    fun enqueueErrorResponse(server: MockWebServer) {
        server.enqueue(MockResponse().setResponseCode(404))
    }

    fun clickSearchBottomBar() = onView(withId(R.id.searchFragment)).perform(ViewActions.click())
}