package com.ciandt.book.seeker.home.listing

import androidx.test.rule.ActivityTestRule
import com.ciandt.book.seeker.BookSeekerTestApplication
import com.ciandt.book.seeker.home.common.BaseRobot
import com.ciandt.book.seeker.home.common.TestData
import com.ciandt.book.seeker.ui.HomeActivity
import okhttp3.mockwebserver.MockResponse
import org.junit.Rule
import org.junit.Test

class BookListingTest {

    @Rule
    @JvmField
    val rule = ActivityTestRule(HomeActivity::class.java, true, true)

    @Test
    fun when_startListing_should_displayLoading() {
        BaseRobot {
            enqueueSuccessResponseWithThrottle((rule.activity.application as BookSeekerTestApplication).getServer())
        }
        BookListingRobot {
            isLoadingStateDisplayed()
        }
    }

    @Test
    fun when_startScreen_withError_should_displayErrorScreen() {
        BaseRobot {
            enqueueErrorResponse((rule.activity.application as BookSeekerTestApplication).getServer())
        }

        BookListingRobot {
            isErrorStateDisplayed()
        }
    }

    @Test
    fun when_startScreen_withError_should_retry() {
        BaseRobot {
            enqueueErrorResponse((rule.activity.application as BookSeekerTestApplication).getServer())
            enqueueSuccessResponse((rule.activity.application as BookSeekerTestApplication).getServer())
        }

        BookListingRobot {
            isErrorStateDisplayed()
            clickRetryButton()
            isBookListDisplayed()
        }
    }

    @Test
    fun when_startScreen_should_displayBooks() {
        BaseRobot {
            enqueueSuccessResponse((rule.activity.application as BookSeekerTestApplication).getServer())
        }

        BookListingRobot {
            isBookListDisplayed()
        }
    }

    @Test
    fun when_clickOnItem_should_goToDetails() {

        BaseRobot {
            enqueueSuccessResponse((rule.activity.application as BookSeekerTestApplication).getServer())
        }

        (rule.activity.application as BookSeekerTestApplication).getServer()
            .enqueue(MockResponse().setResponseCode(200).setBody(TestData.LISTING_RESPONSE))

        BookListingRobot {
            clickListItem()
            isBookDetailsDisplayed()
        }
    }

}